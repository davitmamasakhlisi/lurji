// Desktop Slider JS

$('.slide-main').owlCarousel({
    items: 1,
    nav: false,
    responsiveClass: true,

    responsive: {
        0: {
            items: 1,
        },
        600: {
            items: 1,
        },
        1000: {
            items: 1,
        }
    }
});

// Mobile Slider JS

$('.slide-main-mobile').owlCarousel({
loop: true,
items: 1,
nav: false,
responsiveClass: true
});

// Deals JS

$('.deals-carousel').owlCarousel({
items: 5,
loop: true,
margin: 30,
nav: false,
responsiveClass: true,

responsive: {
    0: {
        items: 2.25,
        margin: 10,
    },
    576: {
        items: 2,
    },
    768: {
        items: 3,
    },
    992: {
        items: 4,
    },
    1200: {
        items: 5,
    }
}
});

// Deals 2 JS

$('.deals-carousel-2').owlCarousel({
items: 5,
loop: true,
margin: 30,
nav: false,
responsiveClass: true,

responsive: {
    0: {
        items: 2.25,
        margin: 10,
    },
    576: {
        items: 2,
    },
    768: {
        items: 3,
    },
    992: {
        items: 4,
    },
    1200: {
        items: 5,
    }
}
});

// Deals 3 JS

$('.deals-carousel-3').owlCarousel({
items: 5,
loop: true,
margin: 30,
nav: false,
responsiveClass: true,

responsive: {
    0: {
        items: 2.25,
        margin: 10,
    },
    576: {
        items: 2,
    },
    768: {
        items: 3,
    },
    992: {
        items: 4,
    },
    1200: {
        items: 5,
    }
}
});

// Deals Favorite Items
$('.favorite-item').owlCarousel({
loop: true,
margin: 10,
nav: false,
responsiveClass: true,

responsive: {
    0: {
        items: 2.25,
        margin: 10,
    },
    576: {
        items: 2.25,
    },
    768: {
        items: 3.25,
    },
    992: {
        items: 3.25,
    },
    1200: {
        items: 3.25,
    },
    1360: {
        items: 4.25,
    }
}
});

// Search Category Container

$('.choosen_category').click(function() {

$('.option_category').css('display', 'block');

$('.choosen_main img').css('transform', 'rotate(180deg)');

$this = $('.choose_category li');

$this.click(function() {

    $this.removeClass('active_cat');

    $(this).addClass('active_cat');

    $('.choosen_category').text($(this).text());

    $('.option_category').css('display', 'none');

    $('.choosen_main img').css('transform', 'rotate(0deg)');

});

});

// Dynamic Mobile Menu

$('.menu_next').click(function(e) {
e.preventDefault();

var parent_li = $(this).parent().parent().data('id');

$('.parent_ul').css({

    left: '-250px'

});

$(this).parent().parent().parent().parent().find(`[data-id='${parent_li}']`).css({

    left: '0px'

});

});

$('.menu_prev').click(function(e) {
e.preventDefault();

$('.children_ul').css({
    left: '250px'
});

$('.parent_ul').css({
    left: '0px'
});

});

var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
acc[i].addEventListener("click", function() {
    this.classList.toggle("active_sub_menu");
    var panel = this.parentElement.parentElement.nextElementSibling;
    if (panel.style.maxHeight) {
        panel.style.maxHeight = null;
    } else {
        panel.style.maxHeight = panel.scrollHeight + "px";
    }
});
}

//  Dynamic Sub Menu

$(".menu_list ul li")
.mouseenter(function() {
    $(this).addClass('active_submenu');
    $(this).children().next('div').css('display', 'flex');
})
.mouseleave(function() {
    $(this).removeClass('active_submenu');
    $(this).children().next('div').css('display', 'none');
});

function openNav() {

setTimeout(function() {
    $('#mySidenav').css({
        zIndex: '111',
        left: '0px'
    });
}, 0);

$('html').css({
    overflow: 'hidden',
    height: '100%'
});
$('#body').css({
    overflow: 'hidden',
    height: '100%',
    left: '250px'
});

$('.blackscreen').css({
    width: '100%',
    opacity: '1'
});

}

function closeNav() {

setTimeout(function() {
    $('#mySidenav').css({
        left: '-250px'

    });
}, 0);

$('html').css({
    overflow: 'visible',
    height: '100%'
});
$('#body').css({
    overflow: 'visible',
    height: '100%',
    left: '0px'
});

$('.blackscreen').css({

    opacity: '0'
});

setTimeout(function() {

    $('.blackscreen').css({
        width: '0',
    });

}, 500);

}

$('.blackscreen').click(function() {

$(this).css({

    opacity: '0'
});

if ($('.search_outer_cont').hasClass('active_search')) {

    xSearch();

    $('.textarea_inner').css('display', 'none');

}

$('.textarea_inner').css('display', 'none');

setTimeout(function() {

    $('.blackscreen').css({
        width: '0',
    });

}, 500);

$('.filter_side').css({
    right: '-250px'
});

$('#mySidenav').css({
    left: '-250px',

});

$('html').css({
    overflow: 'visible',
    height: '100%'
});
$('#body').css({
    overflow: 'visible',
    height: '100%',
    transition: '0.5s',
    left: '0px'
});

});

$(".search-field").keypress(function() {

$('.textarea_inner').css('display', 'block');

$('.blackscreen').css({

    opacity: '1',

    width: '100%'

});

});

function search_mobile() {

$('.search_container').css({
    'display': 'block'

});

$('.search_outer_cont').addClass('active_search');

setTimeout(function() {

    $('.search_outer_cont').css({
        'top': '47px',
    });

}, 300);

setTimeout(function() {

    $('.search_outer_cont').css({
        'z-index': '10',
    });

}, 350);

setTimeout(function() {

    $('.blackscreen').css({

        opacity: '1',

        width: '100%'

    });

}, 400);

$('html').css({
    overflow: 'hidden',
    height: '100%'
});
$('#body').css({
    overflow: 'hidden',
    height: '100%'
});

}

function xSearch() {

$('.textarea_inner').css('display', 'none');

$('.search_outer_cont').css({
    'z-index': '8',
    'top': '0px'

});

setTimeout(function() {
    $('.search_container').css('display', 'none');

}, 350);

$('.blackscreen').css({
    opacity: '0'
});

$('.search_outer_cont').removeClass('active_search');

$('html').css({
    overflow: 'visible',
    height: '100%'
});
$('#body').css({
    overflow: 'visible',
    height: '100%'
});

}

$('.basket').click(function() {

$('.blackscreen2').css({
    opacity: '1',
    width: '100%'
});

$('.search-main').css('z-index', '9');

$('.cart_modal').css({
    right: '0px'
});

$('html').css({
    overflow: 'hidden',
    height: '100%'
});

$('#body').css({
    overflow: 'hidden',
    height: '100%'
});

$('#body').toggleClass('scroll_padding');

});

$('.blackscreen2').click(function() {

$(this).css({

    opacity: '0'
});

setTimeout(function() {

    $('.blackscreen2').css({
        width: '0',
    });

}, 500);

$('.cart_modal').css({
    right: '-555px'
});

$('html').css({
    overflow: 'visible',
    height: '100%'
});

$('#body').css({
    overflow: 'visible',
    height: '100%',
    transition: 'unset'

});

setTimeout(function() {

    $('#body').css({
        transition: '0.5s'
    });
}, 500);

$('.search-main').css('z-index', '11');

$('#body').toggleClass('scroll_padding');

});

$('.cr_x_button').click(function() {

$('.cart_modal').css({
    right: '-555px'
});

$('html').css({
    overflow: 'visible',
    height: '100%'
});
$('#body').css({
    overflow: 'visible',
    height: '100%',
    transition: 'unset'

});

setTimeout(function() {

    $('#body').css({
        transition: '0.5s'
    });
}, 500);

$('.blackscreen2').css({
    opacity: '0'
});

setTimeout(function() {
    $('.search-main').css('z-index', '11');
    $('.blackscreen2').css({
        width: '0',
    });

}, 500);

$('#body').toggleClass('scroll_padding');

});

$(document).ready(function() {

if (!$('.check_product').children('div').hasClass('cr_modal_product')) {

    $('.if_product_not').css('display', 'none');
    $('.product_not').css('display', 'block');

} else {
    $('.if_product_not').css('display', 'block');
    $('.product_not').css('display', 'none');
}
});
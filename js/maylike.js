// Deals 3 JS

$('.maylike-carousel').owlCarousel({
    items:5,
    loop:true,
    margin:30,
    nav:false,
    responsiveClass:true,

    responsive:{
    0:{
    items:2.25,
    margin:10,
    },
    576:{
    items:2,
    },
    768:{
    items:3,
    },
    992:{
    items:4,
    },
    1200:{
    items:5,
    }
    }
    });



    
function countItems(){

const counts = document.querySelectorAll('.count_item');

counts.forEach( (count) => {

const currPrice = parseFloat(count.parentElement.parentElement.nextElementSibling.children.item(0).innerText);
const currPriceElement = count.parentElement.parentElement.nextElementSibling.children.item(0);

const innerCount = parseFloat(count.innerText);

const resultPrice = currPrice * innerCount;

currPriceElement.innerHTML = `${resultPrice}<span class="lari">'</span>`;

});

}


countItems();



function countSecondary(){

const count_sec = document.querySelectorAll('.count_secondary');

count_sec.forEach( (count) => {

    const curr_sec = parseFloat(count.parentElement.nextElementSibling.innerText);

    const curr_sec_text = count.parentElement.nextElementSibling;

    const curr_count = count.innerText;

    const result = curr_sec * curr_count;

    curr_sec_text.innerHTML = `${result}<span class="lari">'</span>`;




});

}


countSecondary();





    


    $('.delete_desk').click(function(){

    const detectItem = $(this).parent().parent().parent().data('id');

    $(this).parent().parent().parent().remove();

    $(`.cr_modal_product[data-id='${detectItem}']`).remove();

    checkProduct();
    checkItems();
    countItem();
    sumItem();

    });



    $('.delete_mob').click(function(){

    const detectItem = $(this).parent().parent().data('id');

    $(this).parent().parent().remove();

    $(`.cr_modal_product[data-id='${detectItem}']`).remove();

    checkProduct();

    checkItems();

    countItem();
    
    sumItem();
    

    });



    


    function countItem(){

    const itemLength = $('.flex_court').length;

    $('.busket_count').text(itemLength);


    }

    countItem();




    function sumItem(){

    const itemsPrice = document.querySelectorAll('.cart_pr_detail p');

    let allPrice = 0;

    itemsPrice.forEach( (item) => {

    const innerNum = parseFloat(item.innerText);

    allPrice += innerNum;

    });


    showAllPrice(allPrice.toFixed(2));
    monthPrice((allPrice / 100 * 4).toFixed(2));


    }

    sumItem();


    function showAllPrice(result){

    const allResult = document.querySelector('.cart_res_num');

    const allResultSecond = document.querySelector('.cart_sum_num');

    const allResultThird = document.querySelector('.cr_sum_num');

    const html = `<p class="cart_res_num">${result}<span class="lari">'</span></p>`;

    const htmlSecond = `<p class="cart_sum_num">${result}<span class="lari">'</span></p>`;

    const htmlThird = `<p class="cr_sum_num">${result}<span class="lari">'</span></p>`;



    allResult.innerHTML = html;
    allResultSecond.innerHTML = htmlSecond;
    allResultThird.innerHTML = htmlThird;

    }



    function monthPrice(result){

    const monthPr = document.querySelector('.month_pr');

    const html = `<span class="month_pr">${result}</span>`;

    monthPr.innerHTML = html;


    }


    function checkItems(){

    if( !$('.left_basket').children('div').hasClass('flex_court') ){

    $('.empty_item_row').css('display','block');

    $('.product_row').css('display','none');

    }else{
    $('.empty_item_row').css('display','none');

    $('.product_row').css('display','flex');
    }

    }


    checkItems();




function changeItem(){


$('.blackscreen2').css({
opacity: '1',
width: '100%'
});


$('.search-main').css('z-index','9');

$('.change_item').css({
right: '0px'	
});


$('html').css({
overflow:'hidden',
height: '100%'
});

$('#body').css({
overflow:'hidden',
height: '100%'
});

$('#body').toggleClass('scroll_padding');




}	


function changeClose(){

$('.change_item').css({
right: '-555px'	
});


$('html').css({
overflow:'visible',
height: '100%'
});
$('#body').css({
overflow:'visible',
height: '100%',
transition: 'unset'

});

setTimeout(function(){

$('#body').css({
transition: '0.5s'				
});
}, 500);




$('.blackscreen2').css({
opacity: '0'
});

setTimeout(function(){
$('.search-main').css('z-index','11');
$('.blackscreen2').css({
width: '0',
});


}, 500);


$('#body').toggleClass('scroll_padding');


}



$('.quantity_change_cart').click(function(e){
    e.preventDefault();
    e.stopPropagation();
    

    if(!$('.change_color_content').hasClass('active_chng_color')){

        $('.change_quant_content').toggleClass('active_chng_quant');


    }

    


});


$(body).click(function(){

    

    $('.change_quant_content').removeClass('active_chng_quant');
    $('.change_color_content').removeClass('active_chng_color');

});


$('.change_quant_content ul li').click(function(){

$('.active_quant').removeClass('active_quant');
$(this).addClass('active_quant');
    

const currQuant = $(this).text();

$('.quantity_change_cart p span').text(currQuant);






});


$('.color_change_cart').click(function(e){
    e.preventDefault();
    e.stopPropagation();
    

    $('.change_color_content').toggleClass('active_chng_color');


});


$('.change_color_content ul li').click(function(){


$('.active_color').removeClass('active_color');
$(this).addClass('active_color');
const currQuant = $(this).text();



$('.color_change_cart p span').text(currQuant);



});